﻿#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

class Player 
{
    public:
    string name;
    int score;
};

void sortPlayersByScore(Player* players, int size) 
{
    for (int i = 0; i < size - 1; i++) 
    {
        for (int j = 0; j < size - i - 1; j++) 
        {
            if (players[j].score < players[j + 1].score) 
            {
                Player temp = players[j];
                players[j] = players[j + 1];
                players[j + 1] = temp;
            }
        }
    }
}

int main() 
{
    setlocale(LC_ALL, "Russian");
    int numPlayers;
    cout << "Введите количество игроков: ";
    cin >> numPlayers;

    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; i++) 
    {
        cout << "Введите имя игрока " << i + 1 << ": ";
        cin >> players[i].name;
        cout << "Введите счет игрока " << i + 1 << ": ";
        cin >> players[i].score;
    }

    sortPlayersByScore(players, numPlayers);

    cout << "Итоговая таблица:\n";
    for (int i = 0; i < numPlayers; i++) 
    {
        cout << setw(15) << players[i].name << setw(3) << " - " << setw(8) << players[i].score << endl;
    }

    delete[] players;

    return 0;
}